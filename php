<?php


    function verisoma($x, $y, $z){

        if ( ($x + $y) + $z == $x + ($y + $z) &&
             $x + $y == $y + $x &&
             $x + 0 == $x && $y + 0 == $y &&
             ($x + (-$x) == 0) && ($y + (-$y) == 0) )
          {
            return true;
          }
        return false;
    }

    function veriproduto($x, $y, $z){

        if ( ($x * $y) * $z == $x * ($y * $z) &&
             $x * $y == $y * $x &&
             $x * 1 == $x && $y * 1 == $y &&
             $x * (1/$x) == 1 && $y * (1/$y) == 1 )
          {
              return true;
          }
    return false;
    }

    function soma($x, $y){
        $z = 10;
        if(verisoma($x, $y, $z))
        {
            return $x + $y;
        }
            return "Error! A Soma está inválida!";
    }

    function produto($x, $y){
        $z = 10;
        if(veriproduto($x, $y, $z))
        {
            return $x * $y;
        }
            return "Error! O produto está inválido!"
    }

    $x = $_GET["a"];
    $y = $_GET["b"];

    $Soma = soma($x, $y);
    $Produto = produto($x, $y);
 
    echo "A soma entre $x e $y é igual a $Soma";
    echo "O produto dos números $x e $y é igual a $Produto";

?>
